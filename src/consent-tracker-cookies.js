import Cookie from 'js-cookie';

const CONSENT_COOKIE_NAME = '@@consentTracker';

class ConsentTrackerCookies {

  setConfig(config) {
    this.config = config;
  }

  setConsent(consent) {
    if (!!consent || this.config && this.config.setDeclineCookie !== false) {
      Cookie.set(CONSENT_COOKIE_NAME, !!consent);
    }
  }
  
  checkConsentCookie() {
    return Cookie.getJSON(CONSENT_COOKIE_NAME);
  }
}

export default ConsentTrackerCookies;
