import ExecutionEnvironment from 'exenv';

class ConsentTrackerEvents {

  constructor() {
    this.loadedScripts = [];
    this.callbackRuns = [];
    this.trackingQue = [];
  }

  init() {
    if (this.config && !this.loader && ExecutionEnvironment.canUseDOM && this.checkConsent()) {
      this.loader = require('scriptjs');
      this.loadScripts();
      this.runCallbacks();
    }
  }

  setConfig(config) {
    this.config = config;
  }

  setConsent(consent=true) {
    this.consented = consent;
    this.checkQue();
  }

  trackEvent(event, argObj) {
    if (this.config && this.config.events && this.config.events[event]) {
      let eventArr;
      if (Array.isArray(this.config.events[event])) {
        eventArr = this.config.events[event];
      } else {
        eventArr = Object.keys(this.config.events[event]).map(
          key => [key, ...this.config.events[event][key]]
        );
      }
      eventArr.map(t => this.trackSingle(...t, ...(argObj && argObj[t[0]] ? argObj[t[0]] : [])));
    }
  }

  trackSingle(label, func, ...args) {
    this.init();
    if (this.scriptIsLoaded(label) && this.checkConsent(label)) {
      this.execute(func, null, ...args);
    } else {
      this.addToQue(label, func, ...args);
    }
  }

  loadScripts() {
    if (this.config && this.config.scripts) {
      Object.keys(this.config.scripts).map(
        key => this.addScript(key, this.config.scripts[key])
      );
    }
  }

  addScript(label, url) {
    this.init();
    if (!this.scriptIsLoaded(label)) {
      this.loader(url, label, () => {
        this.loadedScripts.push(label);
        this.checkQue();
      });
    }
  }

  runCallbacks() {
    if (this.config && this.config.callbacks) {
      Object.keys(this.config.callbacks).map(
        key => {
          this.config.callbacks[key]();
          this.callbackRuns.push(key);
        }
      );
      this.checkQue();
    }
  }

  addToQue() {
    this.trackingQue.push(arguments);
  }

  checkQue() {
    this.init();
    this.trackingQue = this.trackingQue.filter(t => {
      if (this.scriptIsLoaded(t[0])) {
        this.trackSingle(...t);
        return false;
      } else {
        return true;
      }
    });
  }

  execute(functionName, context) {
    var args = Array.prototype.slice.call(arguments, 2),
        context = !context && typeof window !== 'undefined' ? window : context,
        namespaces = functionName.split("."),
        func = namespaces.pop();
    for(var i = 0; i < namespaces.length; i++) {
      context = context[namespaces[i]];
    }
    return context[func].apply(context, args);
  }

  scriptIsLoaded(label) {
    if (this.config.scopes && this.config.scopes[label] && this.config.scopes[label].dependencies) {
      return  Array.isArray(this.config.scopes[label].dependencies)
              ? this.config.scopes[label].dependencies.filter(dep => this.scriptIsLoaded(dep)).length >= this.config.scopes[label].dependencies.length
              : this.scriptIsLoaded(this.config.scopes[label].dependencies);
    }
    return  this.config.scripts && this.config.scripts[label] && this.loadedScripts.indexOf(label) >= 0 ||
            this.config.callbacks && this.config.callbacks[label] && this.callbackRuns.indexOf(label) >= 0;
  }

  checkConsent(scope) {
    return !this.checkRequiredConsent(scope) || !!this.consented;
  }

  checkRequiredConsent(scope) {
    if (!this.config) {
      return false;
    }
    if (this.config.requireConsent) {
      return true;
    }
    if (!scope && this.config.scopes) {
      return !!Object.keys(this.config.scopes).filter(
        key => !!this.config.scopes[key].requireConsent
      ).length;
    }
    if (scope && this.config && this.config.scopes && this.config.scopes[scope]) {
      var check = this.config.scopes[scope]
      if (check.requireConsent) {
        return true;
      }
      if (check.dependencies) {
        if (Array.isArray(check.dependencies)) {
          return !!check.dependencies.filter(dep => this.checkRequiredConsent(dep)).length;
        } else {
          return this.checkRequiredConsent(check.dependencies);
        }
      }
    }
    return false;
  }

  checkDecided() {
    return typeof this.consented !== 'undefined';
  }
}

export default ConsentTrackerEvents;
