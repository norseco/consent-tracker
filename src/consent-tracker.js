import ConsentTrackerEvents from './consent-tracker-events';
import ConsentTrackerCookies from './consent-tracker-cookies';

export class ConsentTracker {

  constructor(config) {
    this.events = new ConsentTrackerEvents();
    this.cookie = new ConsentTrackerCookies();
    this.setConfig(config);
    this.checkConsent();
  }

  setConfig(config) {
    this.config = config;
    this.events.setConfig(config);
    this.cookie.setConfig(config);
    this.init();
  }

  init() {
    const c = this.cookie.checkConsentCookie();
    if (typeof c !== 'undefined') {
      this.setConsent(c);
    }
  }

  setConsent(consent=true) {
    this.cookie.setConsent(consent);
    this.events.setConsent(consent);
  }

  trackEvent() {
    this.events.trackEvent(...arguments);
  }

  trackSingle() {
    this.events.trackSingle(...arguments);
  }

  checkConsent() {
    return this.events.checkConsent();
  }

  checkRequiredConsent() {
    return this.events.checkRequiredConsent();
  }

  checkDecided() {
    return this.events.checkDecided();
  }
}
