"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _jsCookie = _interopRequireDefault(require("js-cookie"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var CONSENT_COOKIE_NAME = '@@consentTracker';

var ConsentTrackerCookies =
/*#__PURE__*/
function () {
  function ConsentTrackerCookies() {
    _classCallCheck(this, ConsentTrackerCookies);
  }

  _createClass(ConsentTrackerCookies, [{
    key: "setConfig",
    value: function setConfig(config) {
      this.config = config;
    }
  }, {
    key: "setConsent",
    value: function setConsent(consent) {
      if (!!consent || this.config && this.config.setDeclineCookie !== false) {
        _jsCookie.default.set(CONSENT_COOKIE_NAME, !!consent);
      }
    }
  }, {
    key: "checkConsentCookie",
    value: function checkConsentCookie() {
      return _jsCookie.default.getJSON(CONSENT_COOKIE_NAME);
    }
  }]);

  return ConsentTrackerCookies;
}();

var _default = ConsentTrackerCookies;
exports.default = _default;