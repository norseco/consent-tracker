"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ConsentTracker = void 0;

var _consentTrackerEvents = _interopRequireDefault(require("./consent-tracker-events"));

var _consentTrackerCookies = _interopRequireDefault(require("./consent-tracker-cookies"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var ConsentTracker =
/*#__PURE__*/
function () {
  function ConsentTracker(config) {
    _classCallCheck(this, ConsentTracker);

    this.events = new _consentTrackerEvents.default();
    this.cookie = new _consentTrackerCookies.default();
    this.setConfig(config);
    this.checkConsent();
  }

  _createClass(ConsentTracker, [{
    key: "setConfig",
    value: function setConfig(config) {
      this.config = config;
      this.events.setConfig(config);
      this.cookie.setConfig(config);
      this.init();
    }
  }, {
    key: "init",
    value: function init() {
      var c = this.cookie.checkConsentCookie();

      if (typeof c !== 'undefined') {
        this.setConsent(c);
      }
    }
  }, {
    key: "setConsent",
    value: function setConsent() {
      var consent = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
      this.cookie.setConsent(consent);
      this.events.setConsent(consent);
    }
  }, {
    key: "trackEvent",
    value: function trackEvent() {
      var _this$events;

      (_this$events = this.events).trackEvent.apply(_this$events, arguments);
    }
  }, {
    key: "trackSingle",
    value: function trackSingle() {
      var _this$events2;

      (_this$events2 = this.events).trackSingle.apply(_this$events2, arguments);
    }
  }, {
    key: "checkConsent",
    value: function checkConsent() {
      return this.events.checkConsent();
    }
  }, {
    key: "checkRequiredConsent",
    value: function checkRequiredConsent() {
      return this.events.checkRequiredConsent();
    }
  }, {
    key: "checkDecided",
    value: function checkDecided() {
      return this.events.checkDecided();
    }
  }]);

  return ConsentTracker;
}();

exports.ConsentTracker = ConsentTracker;