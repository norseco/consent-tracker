"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _exenv = _interopRequireDefault(require("exenv"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var ConsentTrackerEvents =
/*#__PURE__*/
function () {
  function ConsentTrackerEvents() {
    _classCallCheck(this, ConsentTrackerEvents);

    this.loadedScripts = [];
    this.callbackRuns = [];
    this.trackingQue = [];
  }

  _createClass(ConsentTrackerEvents, [{
    key: "init",
    value: function init() {
      if (this.config && !this.loader && _exenv.default.canUseDOM && this.checkConsent()) {
        this.loader = require('scriptjs');
        this.loadScripts();
        this.runCallbacks();
      }
    }
  }, {
    key: "setConfig",
    value: function setConfig(config) {
      this.config = config;
    }
  }, {
    key: "setConsent",
    value: function setConsent() {
      var consent = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
      this.consented = consent;
      this.checkQue();
    }
  }, {
    key: "trackEvent",
    value: function trackEvent(event, argObj) {
      var _this = this;

      if (this.config && this.config.events && this.config.events[event]) {
        var eventArr;

        if (Array.isArray(this.config.events[event])) {
          eventArr = this.config.events[event];
        } else {
          eventArr = Object.keys(this.config.events[event]).map(function (key) {
            return [key].concat(_toConsumableArray(_this.config.events[event][key]));
          });
        }

        eventArr.map(function (t) {
          return _this.trackSingle.apply(_this, _toConsumableArray(t).concat(_toConsumableArray(argObj && argObj[t[0]] ? argObj[t[0]] : [])));
        });
      }
    }
  }, {
    key: "trackSingle",
    value: function trackSingle(label, func) {
      this.init();

      for (var _len = arguments.length, args = new Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
        args[_key - 2] = arguments[_key];
      }

      if (this.scriptIsLoaded(label) && this.checkConsent(label)) {
        this.execute.apply(this, [func, null].concat(args));
      } else {
        this.addToQue.apply(this, [label, func].concat(args));
      }
    }
  }, {
    key: "loadScripts",
    value: function loadScripts() {
      var _this2 = this;

      if (this.config && this.config.scripts) {
        Object.keys(this.config.scripts).map(function (key) {
          return _this2.addScript(key, _this2.config.scripts[key]);
        });
      }
    }
  }, {
    key: "addScript",
    value: function addScript(label, url) {
      var _this3 = this;

      this.init();

      if (!this.scriptIsLoaded(label)) {
        this.loader(url, label, function () {
          _this3.loadedScripts.push(label);

          _this3.checkQue();
        });
      }
    }
  }, {
    key: "runCallbacks",
    value: function runCallbacks() {
      var _this4 = this;

      if (this.config && this.config.callbacks) {
        Object.keys(this.config.callbacks).map(function (key) {
          _this4.config.callbacks[key]();

          _this4.callbackRuns.push(key);
        });
        this.checkQue();
      }
    }
  }, {
    key: "addToQue",
    value: function addToQue() {
      this.trackingQue.push(arguments);
    }
  }, {
    key: "checkQue",
    value: function checkQue() {
      var _this5 = this;

      this.init();
      this.trackingQue = this.trackingQue.filter(function (t) {
        if (_this5.scriptIsLoaded(t[0])) {
          _this5.trackSingle.apply(_this5, _toConsumableArray(t));

          return false;
        } else {
          return true;
        }
      });
    }
  }, {
    key: "execute",
    value: function execute(functionName, context) {
      var args = Array.prototype.slice.call(arguments, 2),
          context = !context && typeof window !== 'undefined' ? window : context,
          namespaces = functionName.split("."),
          func = namespaces.pop();

      for (var i = 0; i < namespaces.length; i++) {
        context = context[namespaces[i]];
      }

      return context[func].apply(context, args);
    }
  }, {
    key: "scriptIsLoaded",
    value: function scriptIsLoaded(label) {
      var _this6 = this;

      if (this.config.scopes && this.config.scopes[label] && this.config.scopes[label].dependencies) {
        return Array.isArray(this.config.scopes[label].dependencies) ? this.config.scopes[label].dependencies.filter(function (dep) {
          return _this6.scriptIsLoaded(dep);
        }).length >= this.config.scopes[label].dependencies.length : this.scriptIsLoaded(this.config.scopes[label].dependencies);
      }

      return this.config.scripts && this.config.scripts[label] && this.loadedScripts.indexOf(label) >= 0 || this.config.callbacks && this.config.callbacks[label] && this.callbackRuns.indexOf(label) >= 0;
    }
  }, {
    key: "checkConsent",
    value: function checkConsent(scope) {
      return !this.checkRequiredConsent(scope) || !!this.consented;
    }
  }, {
    key: "checkRequiredConsent",
    value: function checkRequiredConsent(scope) {
      var _this7 = this;

      if (!this.config) {
        return false;
      }

      if (this.config.requireConsent) {
        return true;
      }

      if (!scope && this.config.scopes) {
        return !!Object.keys(this.config.scopes).filter(function (key) {
          return !!_this7.config.scopes[key].requireConsent;
        }).length;
      }

      if (scope && this.config && this.config.scopes && this.config.scopes[scope]) {
        var check = this.config.scopes[scope];

        if (check.requireConsent) {
          return true;
        }

        if (check.dependencies) {
          if (Array.isArray(check.dependencies)) {
            return !!check.dependencies.filter(function (dep) {
              return _this7.checkRequiredConsent(dep);
            }).length;
          } else {
            return this.checkRequiredConsent(check.dependencies);
          }
        }
      }

      return false;
    }
  }, {
    key: "checkDecided",
    value: function checkDecided() {
      return typeof this.consented !== 'undefined';
    }
  }]);

  return ConsentTrackerEvents;
}();

var _default = ConsentTrackerEvents;
exports.default = _default;