"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _consentTracker = require("./consent-tracker");

var _default = new _consentTracker.ConsentTracker();

exports.default = _default;