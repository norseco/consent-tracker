# Tracker

Library for configuring and handling tracking code across with user consent.

### Installation

```sh
$ npm install --save bitbucket:norseco/consent-tracker
```

### Tracker

```javascript
import Tracker from 'norse-consent';
Tracker.setConfig(config);
```

```javascript
// Config example
{
    callbacks: {
        <providerName>: () => <code>
    },
    scripts: {
        <providerName>: <scriptUrl>
    },
    events: {
        <eventName>: [
            [<providerName>, <functionName>, <...arguments>],
            [<providerName2>, <functionName2>, <...arguments>]
        ]
    }
}
```

```javascript
Tracker.addScript(<providerName>, <scriptUrl>);
```

```javascript
Tracker.trackEvent(<eventName>);
```

```javascript
Tracker.trackSingle(<providerName>, <functionName>, <...arguments>);
```
